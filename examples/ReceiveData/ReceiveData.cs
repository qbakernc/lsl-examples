using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using LSL;

namespace LSLExamples
{
    static class ReceiveData
    {
        private static ContinuousResolver continuousResolver = new ContinuousResolver();
        private static Dictionary<string, StreamInlet> inletList = new Dictionary<string, StreamInlet>();
        public static void Main(string[] args)
        {
            StreamInfo[] info = LSL.LSL.resolve_stream("type", "EEG", 1, LSL.LSL.FOREVER);
            using StreamInlet inlet = new StreamInlet(info[0], 360, 0, true, processing_options_t.proc_clocksync);
            double timeCorrection = inlet.time_correction();
   
            while (!Console.KeyAvailable)
            {
                float[] sample = new float[8];
                double res = inlet.pull_sample(sample, 0.0f);
                if (res > 0)
                {
                    foreach (float f in sample)
                    {
                        Console.Write("\t{0}", f);
                    }

                    Console.Write("                " + (LSL.LSL.local_clock() - res));
                    Console.WriteLine();
                }
            }
        }
        public static void ClearDiscconnectedStreams()
        {
            StreamInfo[] newStreamInfo = continuousResolver.results();
            string[] sourceId = new string[newStreamInfo.Length];

            for (int i = 0; i < newStreamInfo.Length; i++)
            {
                sourceId[i] = newStreamInfo[i].session_id();
            }

            // Removes all StreamInlets whos StreamInfo is no longer in the StreamInfo array.
            inletList.Where(f => !sourceId.Contains(f.Value.info().session_id())).ToList().ForEach(info =>
            {
                inletList.Remove(info.Value.info().source_id());
            });

            // Creates a new StreamInlet for any new StreamInfo found.
            newStreamInfo.ToList().ForEach(info =>
            {
                if (!inletList.ContainsKey(info.source_id()))
                {
                    inletList.Add(info.source_id(), new StreamInlet(info));
                    Trace.WriteLine(info.as_xml());
                }
            });
        }
    }
}
