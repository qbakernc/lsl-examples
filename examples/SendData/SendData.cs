using System;
using System.Threading;
using LSL;

namespace LSLExamples
{
    static class SendData
    {
        
        public static void Main(string[] args)
        {
            Random rnd = new Random();
            // create stream info and outlet
            Console.WriteLine("Enter type");
            string type = Console.ReadLine();
            using StreamInfo info = new StreamInfo("UnityStream", type, 8, 100, channel_format_t.cf_float32, "UnityStreamID");
            using StreamOutlet outlet = new StreamOutlet(info);
            outlet.wait_for_consumers();
            
            float[] data = new float[8];
            while (outlet.have_consumers())
            {
                Random rand = new Random();
                data[0] = rand.Next(0, 100);
                data[1] = rand.Next(0, 100);
                data[2] = rand.Next(0, 100);
                data[3] = rand.Next(0, 100);
                data[4] = rand.Next(0, 100);
                data[5] = rand.Next(0, 100);
                data[6] = rand.Next(0, 100);
                data[7] = rand.Next(0, 100);
                outlet.push_sample(data, LSL.LSL.local_clock());
              //  Console.WriteLine(LSL.LSL.local_clock());
                Thread.Sleep(10);
            }
            Console.ReadLine();
        }
    }
}
